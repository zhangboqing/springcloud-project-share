package com.zbq.scservicemerchantsapi.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
public interface UserAuthControllerApi {

    @GetMapping("ignoreAuthentication")
    public String ignoreAuthentication(@RequestParam  String url);

    @GetMapping("getLoginInfo")
    public String getLoginInfo(@RequestParam String token);

    @GetMapping("permission")
    public String permission(@RequestParam String token,@RequestParam String url,@RequestParam String method);
}
