package com.zbq.scservicemerchantsapi.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;


/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
//定义一个feign接口，通过@ FeignClient（“服务名”），来指定调用哪个服务
@Component
@FeignClient(name = "sc-service-merchants",fallback = UserAuthControllerFeignHystric.class)
public interface UserAuthControllerFeignClient extends UserAuthControllerApi {

}
