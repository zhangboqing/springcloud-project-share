package com.zbq.scservicemerchantsapi.controller;

import org.springframework.stereotype.Component;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
@Component
public class UserAuthControllerFeignHystric implements UserAuthControllerFeignClient {

    @Override
    public String ignoreAuthentication( String url) {
        return "fale";
    }

    @Override
    public String getLoginInfo(String token) {
        return "system";
    }

    @Override
    public String permission(String token, String url, String method) {
        return "false";
    }
}
