### 本地调试步骤
```text
一、本地启动步骤
    1、创建网络
     docker network create java_net_dev
    2、修改docker-compose.yml
    替换docker.for.mac.host.internal域名(因为我的是mac环境，容器通过该域名访问本地)，其他环境自行查下域名替换下
    替换/Users/zhangboqing/Downloads/sprongcloud/logs日志路径
    3、docker-compose up

二、相关访问地址
    1、eureka
    http://localhost:8781
    2、zipkin
    http://localhost:9411
    3、sc-base-spring-boot-monitor
    http://localhost:9411
    user/password
    4、rabbitmq
    http://localhost:15672
    guest/guest
    5、prometheus
    http://localhost:9090
    6、grafana
    http://localhost:3000
    admin/admin

三、模块说明
springcloud-project-share：聚合模块（提供聚合子模块的功能，方便统一编译打包）
    sc-base-config-center：配置中心（存放配置的地方）
    sc-base-config-server：配置服务（提供读取配置中心配置的能力）
    sc-base-eureka-server：注册中心（提供服务注册和发现的地方）
    sc-base-gateway：api网关（提供动态路由等）
    sc-base-spring-boot-monitor：监控中心（基于spring boot actuator信息的简单监控）

    sc-parent：父工程（依赖版本的统一管理）

    sc-service-core：服务core（用于抽取不同服务通用的部分，比如pom依赖，配置等）
    sc-service-goods：goods服务
    sc-service-goods-api：goods api（提供给其他服务通过feign调用goods接口）
    sc-service-merchants：merchants服务（相当于门户应用，早起就这一个服务，其他服务都是从该服务不断拆离出去的）
    sc-service-merchants-api：merchants api
    sc-service-worksheet：worksheet服务
    sc-service-worksheet-api：worksheet api
```