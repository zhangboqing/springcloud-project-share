package com.zbq.scserviceworksheetapi.controller;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
@Component
public class WorksheetControllerFeignHystric implements WorksheetControllerFeignClient {

    @Override
    public String method1() {
        return "method1 Hystric fallback";
    }

    @Override
    public String method2(String param) {
        return "method2 Hystric fallback";
    }

    @Override
    public String method3(Map param) {
        return "method3 Hystric fallback";
    }

    @Override
    public String goodsMethod1() {
        return "goodsMethod1 Hystric fallback";
    }

    @Override
    public String slowGoodsMethod1() {
        return "slowGoodsMethod1 Hystric fallback";
    }

}
