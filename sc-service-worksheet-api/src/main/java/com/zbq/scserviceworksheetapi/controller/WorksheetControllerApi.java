package com.zbq.scserviceworksheetapi.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
public interface WorksheetControllerApi {

    @GetMapping("/worksheet/method1")
    public String method1();

    @GetMapping("/worksheet/method2")
    public String method2(@RequestParam("param") String param);

    @PostMapping("/worksheet/method3")
    public String method3(@RequestBody Map param);

    @GetMapping("/worksheet/goods/method1")
    public String goodsMethod1();

    @GetMapping("/worksheet/goods/method2")
    public String slowGoodsMethod1();


}
