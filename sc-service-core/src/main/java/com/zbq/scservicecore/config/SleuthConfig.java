package com.zbq.scservicecore.config;

import brave.sampler.Sampler;
import feign.Logger;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.sleuth.sampler.ProbabilityBasedSampler;
import org.springframework.cloud.sleuth.sampler.SamplerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhangboqing
 * @date 2020/06/23
 */
@Configuration
@EnableConfigurationProperties(SamplerProperties.class)
public class SleuthConfig {

    @Bean
    public Sampler probabilityBasedSampler(SamplerProperties configuration) {
        return  new ProbabilityBasedSampler(configuration);
    }
}
