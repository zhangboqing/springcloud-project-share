package com.zbq.scservicemerchants.controller;

import brave.Tracing;
import brave.propagation.CurrentTraceContext;
import brave.propagation.TraceContext;
import com.netflix.client.http.HttpHeaders;
import com.netflix.client.http.HttpRequest;
import com.netflix.discovery.converters.Auto;
import com.zbq.scserviceworksheetapi.controller.WorksheetControllerFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhangboqing
 * @Date 2020/7/21
 */
@RestController
@Slf4j
public class MerchantsController{

    @Autowired
    private WorksheetControllerFeignClient worksheetControllerFeignClient;

    @Autowired
    private Tracing tracing;


    @Value("${test_include}")
    private String testInclude;

    @GetMapping("test/include")
    public String testInclude() {
      log.info("testInclude:【{}】",testInclude);
        return testInclude;
    }

    @GetMapping("test")
    public String test() {
        String result = worksheetControllerFeignClient.method1();
        log.info("result:【{}】",result);
        TraceContext traceContext = tracing.currentTraceContext().get();
        return String.valueOf(traceContext.traceIdString());
    }
}
