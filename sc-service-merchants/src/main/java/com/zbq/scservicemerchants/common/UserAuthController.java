package com.zbq.scservicemerchants.common;

import com.zbq.scservicemerchantsapi.controller.UserAuthControllerApi;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhangboqing
 * @Date 2020/7/21
 */
@RestController
public class UserAuthController implements UserAuthControllerApi {


    @Override
    public String ignoreAuthentication(String url) {
        return "false";
    }

    @Override
    public String getLoginInfo(String token) {
        return "zhangboqing";
    }

    @Override
    public String permission(String token, String url, String method) {
        return "true";
    }
}
