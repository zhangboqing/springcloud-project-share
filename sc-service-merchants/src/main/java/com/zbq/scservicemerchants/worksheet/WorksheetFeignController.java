package com.zbq.scservicemerchants.worksheet;

import com.zbq.scserviceworksheetapi.controller.WorksheetControllerApi;
import com.zbq.scserviceworksheetapi.controller.WorksheetControllerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
@RestController
public class WorksheetFeignController implements WorksheetControllerApi {

    @Autowired
    private WorksheetControllerFeignClient worksheetControllerFeignClient;

    @Override
    public String method1() {
        return worksheetControllerFeignClient.method1();
    }

    @Override
    public String method2(String param) {
        return worksheetControllerFeignClient.method2(param);
    }

    @Override
    public String method3(Map param) {
        return worksheetControllerFeignClient.method3(param);
    }

    @Override
    public String goodsMethod1() {
        return worksheetControllerFeignClient.goodsMethod1();
    }

    @Override
    public String slowGoodsMethod1() {
        return worksheetControllerFeignClient.slowGoodsMethod1();
    }

}
