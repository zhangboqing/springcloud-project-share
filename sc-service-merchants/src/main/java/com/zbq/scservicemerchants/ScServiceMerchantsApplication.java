package com.zbq.scservicemerchants;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@EnableEurekaClient
@SpringBootApplication(scanBasePackages = "com.zbq")
public class ScServiceMerchantsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScServiceMerchantsApplication.class, args);
	}

}
