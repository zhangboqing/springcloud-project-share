package com.zbq.scservicegoodsapi.controller;

import org.springframework.stereotype.Component;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
@Component
public class GoodsControllerFeignHystric implements GoodsControllerFeignClient {

    @Override
    public String method1() {
        return null;
    }

    @Override
    public String slowMethod2() {
        return null;
    }
}
