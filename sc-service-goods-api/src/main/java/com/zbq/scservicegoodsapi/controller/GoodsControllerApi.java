package com.zbq.scservicegoodsapi.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
public interface GoodsControllerApi {

    @GetMapping("/goods/method1")
    public String method1();

    @GetMapping("/goods/method2")
    public String slowMethod2();
}
