package com.zbq.scservicegoods.controller;

import com.zbq.scservicegoodsapi.controller.GoodsControllerApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
@RestController
@Slf4j
public class GoodsController implements GoodsControllerApi {

    @Value("${property_string_01}")
    private String propertyString01;

    @Override
    public String method1() {
        log.info("propertyString01:{}",propertyString01);
        return "method1 invoke success, result:"+propertyString01;
    }

    @Override
    public String slowMethod2() {
        log.info("slowMethod2 start");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("slowMethod2 end");
        return "method2 invoke success";
    }

}
