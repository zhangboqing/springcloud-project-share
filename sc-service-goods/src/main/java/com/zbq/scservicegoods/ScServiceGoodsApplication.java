package com.zbq.scservicegoods;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication(scanBasePackages = "com.zbq")
@Slf4j
public class ScServiceGoodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScServiceGoodsApplication.class, args);
		log.info("》》》》》》》》启动成功！！！》》》》》》》》");
	}
}

