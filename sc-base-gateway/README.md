#### 基础配置
```json
[{
    "filters": [
      {
        "args": {
          "regexp": "/goods/(?<segment>.*)",
          "replacement":"/$\\{segment}"
        },
        "name": "RewritePath"
      }
    ],
    "id": "sc-service-goods",
    "order": 0,
    "predicates": [
      {
        "args": {
          "pattern": "/goods/**"
        },
        "name": "Path"
      }
    ],
    "uri": "lb://SC-SERVICE-GOODS"
  },{
    "filters": [
      {
        "args": {
          "regexp": "/worksheet/(?<segment>.*)",
          "replacement":"/$\\{segment}"
        },
        "name": "RewritePath"
      }
    ],
    "id": "sc-service-worksheet",
    "order": 0,
    "predicates": [
      {
        "args": {
          "pattern": "/worksheet/**"
        },
        "name": "Path"
      }
    ],
    "uri": "lb://SC-SERVICE-WORKSHEET"
  }
]
```

### 限流
```json
    [{
        "filters": [
          {
            "args": {
              "regexp": "/goods/(?<segment>.*)",
              "replacement":"/$\\{segment}"
            },
            "name": "RewritePath"
          },
          {
            "args": {
              "key-resolver": "#{@remoteAddrKeyResolver}",
              "redis-rate-limiter.replenishRate": "1",
              "redis-rate-limiter.burstCapacity":"2"
            },
            "name": "RequestRateLimiter"
          }
        ],
        "id": "sc-service-goods",
        "order": 0,
        "predicates": [
          {
            "args": {
              "pattern": "/goods/**"
            },
            "name": "Path"
          }
        ],
        "uri": "lb://SC-SERVICE-GOODS"
      },{
        "filters": [
          {
            "args": {
              "regexp": "/worksheet/(?<segment>.*)",
              "replacement":"/$\\{segment}"
            },
            "name": "RewritePath"
          }
        ],
        "id": "sc-service-worksheet",
        "order": 0,
        "predicates": [
          {
            "args": {
              "pattern": "/worksheet/**"
            },
            "name": "Path"
          }
        ],
        "uri": "lb://SC-SERVICE-WORKSHEET"
      }
    ]
```

### 权重
```json
[{
    "filters": [
      {
        "args": {
          "regexp": "/goods/(?<segment>.*)",
          "replacement":"/$\\{segment}"
        },
        "name": "RewritePath"
      },
      {
        "args": {
          "key-resolver": "#{@remoteAddrKeyResolver}",
          "redis-rate-limiter.replenishRate": "1",
          "redis-rate-limiter.burstCapacity":"2"
        },
        "name": "RequestRateLimiter"
      }
    ],
    "id": "sc-service-goods",
    "order": 0,
    "predicates": [
      {
        "args": {
          "pattern": "/goods/**"
        },
        "name": "Path"
      }
    ],
    "uri": "lb://SC-SERVICE-GOODS"
  },{
    "filters": [
    ],
    "id": "sc-service-worksheet",
    "order": 0,
    "predicates": [
      {
        "args": {
          "pattern": "/worksheet/method"
        },
        "name": "Path"
      },
       {
        "args": {
          "weight.group": "testweight1",
          "weight.weight": "50"
        },
        "name": "Weight"
      }
    ],
    "uri": "http://localhost:8786/worksheet/method1"
  },
  {
    "filters": [
    ],
    "id": "sc-service-worksheet2",
    "order": 0,
    "predicates": [
      {
        "args": {
          "pattern": "/worksheet/method"
        },
        "name": "Path"
      },
       {
        "args": {
          "weight.group": "testweight1",
          "weight.weight": "50"
        },
        "name": "Weight"
      }
    ],
    "uri": "http://localhost:8786/worksheet/method2"
  }
]
```

配置刷新：
http://localhost:8793/goods2/goods/method1