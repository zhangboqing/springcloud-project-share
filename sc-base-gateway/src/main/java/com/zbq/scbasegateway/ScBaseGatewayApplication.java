package com.zbq.scbasegateway;

import com.zbq.scbasegateway.routes.filter.RemoteAddrKeyResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@EnableEurekaClient
//@EnableDynamicConfigEvent
@SpringBootApplication(scanBasePackages = "com.zbq")
@Slf4j
public class ScBaseGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScBaseGatewayApplication.class, args);
		log.info("》》》》》》》》启动成功！！！》》》》》》》》");
	}

}

