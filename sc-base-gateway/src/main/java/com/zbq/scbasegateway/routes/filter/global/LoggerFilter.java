package com.zbq.scbasegateway.routes.filter.global;

import com.alibaba.fastjson.JSONObject;
import com.zbq.scbasegateway.domain.LoggerObject;
import com.zbq.scbasegateway.domain.RequestObject;
import com.zbq.scbasegateway.domain.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Component
@Slf4j
public class LoggerFilter implements GlobalFilter, Ordered {


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        final long start = System.currentTimeMillis();
        ServerHttpResponseDecorator response = response(exchange, start, null);
        return chain.filter(exchange.mutate().response(response).build());
    }

    @Override
    public int getOrder() {
        return -2;
    }

    private ServerHttpResponseDecorator response(ServerWebExchange exchange, long start, String postBody) {
        //对返回对象进行包装
        ServerHttpResponse originalResponse = exchange.getResponse();
        ServerHttpResponseDecorator response = new ServerHttpResponseDecorator(originalResponse) {
            @Override
            public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
                if (body instanceof Flux) {
                    requestAndResponseLogger(exchange.getRequest(), this, start, postBody);
                    return super.writeWith(body);
                }
                return super.writeWith(body);
            }
        };
        return response;
    }

    /**
     * 请求消息和返回消息日志记录，确保日志记录不会印象正常流程
     */
    private void requestAndResponseLogger(ServerHttpRequest requset, ServerHttpResponse response, long start, String body) {
        try {
            LoggerObject loggerObject = new LoggerObject();
            long end = System.currentTimeMillis();
            long time = end - start;
            RequestObject requestObject = new RequestObject();
            requestObject.setMethod(requset.getMethod().name());
            requestObject.setPath(requset.getPath().value());
            requestObject.setHeader(requset.getHeaders().toSingleValueMap());
            requestObject.setParams(requset.getQueryParams().toSingleValueMap());
            requestObject.setBody(body);

            ResponseObject responseObject = new ResponseObject();
            responseObject.setHeader(response.getHeaders().toSingleValueMap());
            responseObject.setStatus(response.getStatusCode().value());
            loggerObject.setTime(time);
            loggerObject.setRequest(requestObject);
            loggerObject.setResponse(responseObject);
            log.info(JSONObject.toJSONString(loggerObject));
        } catch (Exception e) {
            log.error("logger error", e);
        }
    }

}
