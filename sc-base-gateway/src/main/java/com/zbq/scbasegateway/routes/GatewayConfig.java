package com.zbq.scbasegateway.routes;

import com.zbq.scbasegateway.routes.filter.RemoteAddrKeyResolver;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @Author zhangboqing
 * @Date 2020-07-14
 */
@Configuration
public class GatewayConfig {

    @Bean
    @Primary
    @RefreshScope
    public GatewayProperties refreshableGatewayProperties() {
        return new GatewayProperties();
    }


    @Bean(name = RemoteAddrKeyResolver.BEAN_NAME)
    public RemoteAddrKeyResolver remoteAddrKeyResolver() {
        return new RemoteAddrKeyResolver();
    }

}
