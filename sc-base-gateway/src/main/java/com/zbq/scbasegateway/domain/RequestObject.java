package com.zbq.scbasegateway.domain;

import lombok.Data;

import java.util.Map;

@Data
public class RequestObject {

    private String path;

    private String method;

    /**
     * 0 正常 1 测试门店
     */
    private int test;

    private Map<String, String> header;

    private Map<String, String> params;

    private Object body;
}
