package com.zbq.scbasegateway.domain;

import lombok.Data;

/**
 * @author luoyunlong
 * @date 2018/11/6 16:45
 */
@Data
public class IpDomain {

    private Long startIp;

    private Long endIp;
}
