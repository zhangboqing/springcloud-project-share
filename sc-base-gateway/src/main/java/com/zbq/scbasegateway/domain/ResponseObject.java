package com.zbq.scbasegateway.domain;

import lombok.Data;

import java.util.Map;

@Data
public class ResponseObject {

    private Integer status;

    private Map<String, String> header;
}
