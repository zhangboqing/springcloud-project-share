package com.zbq.scbasegateway.domain;

import lombok.Data;

/**
 * 路由返回
 */
@Data
public class ResponseApi {

    private String message;

    private Integer code;

    private Object data;
}
