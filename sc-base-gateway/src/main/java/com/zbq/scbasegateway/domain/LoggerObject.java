package com.zbq.scbasegateway.domain;

import lombok.Data;

@Data
public class LoggerObject {

    private Long time;

    private RequestObject request;

    private ResponseObject response;
}
