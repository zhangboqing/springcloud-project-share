package com.zbq.scserviceworksheet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication(scanBasePackages = "com.zbq")
public class ScServiceWorksheetApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScServiceWorksheetApplication.class, args);
	}
}

