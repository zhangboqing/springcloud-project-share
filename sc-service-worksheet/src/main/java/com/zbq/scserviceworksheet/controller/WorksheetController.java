package com.zbq.scserviceworksheet.controller;

import com.alibaba.fastjson.JSON;
import com.zbq.scservicegoodsapi.controller.GoodsControllerFeignClient;
import com.zbq.scserviceworksheetapi.controller.WorksheetControllerApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author zhangboqing
 * @Date 2020/6/23
 */
@RestController
@Slf4j
public class WorksheetController implements WorksheetControllerApi {

    @Value("${property_int_01}")
    private Integer propertyInt01;

    @Autowired
    private GoodsControllerFeignClient goodsControllerFeignClient;

    @Override
    public String method1() {
        log.info("propertyInt01:{}", propertyInt01);
        return "method1 invoke success, result:" + propertyInt01;
    }

    @Override
    public String method2(String param) {
        log.info("param:{}",param);
        return "worksheet method2 invoke success";
    }

    @Override
    public String method3(Map param) {
        log.info("param:{}", JSON.toJSONString(param));
        return "worksheet method3 invoke success";
    }

    @Override
    public String goodsMethod1() {
        return goodsControllerFeignClient.method1();
    }

    @Override
    public String slowGoodsMethod1() {
        return goodsControllerFeignClient.slowMethod2();
    }


}
